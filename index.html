<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
 "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <title>Faking the obsolete HTML &lt;keygen&gt; element</title>
    <style type="text/css">
      pre { margin: 0.5em; padding: 0.5em; border:1px solid rgba(0,0,0,0.3); background-color: rgba(0,0,0,0.05); }
      img { display: block; margin: 0.5em; }
      summary h3 { display: inline; }
    </style>
  </head>
  <body>
    <h1>Faking the obsolete HTML <code>&lt;keygen&gt;</code> element</h1>
    <p>
      This page presents some tools for dealing with websites
      designed for use with the obsolete
      HTML <code>&lt;keygen&gt;</code> form element. The tools consist
      of a Javascript bookmarklet that transforms the page to fill in
      an answer to the form element, plus some offline Python scripts
      that generate the answer in the correct format.
    </p>
    <p>
      These tools are <strong>early alpha</strong>, not very tested,
      and not streamlined for general use. If you can’t make sense of
      the explanations on this page, they may not be for you.
    </p>
    <h2>Introduction</h2>
    <p>
      Before about 2016, forms in HTML used to be able to contain an
      input element called <code>&lt;keygen&gt;</code>. When a form
      containing this element was submitted to a web server, the
      browser would generate an RSA public/private key pair, and
      upload the public key to the web server as part of the form
      submission. The private key would be retained in the browser’s
      local store of cryptographic material.
    </p>
    <p>
      This system was used by (at least) certification authorities. If
      you were applying for a certificate, the CA’s website might
      present you with an application form including
      a <code>&lt;keygen&gt;</code>, together with further form fields
      for all the rest of the details. You’d submit the whole form in
      one go, and once the CA had done all its checking and
      validation, it would deliver you a certificate matching the
      public key you’d uploaded. Your browser would receive that
      certificate and combine it with the private key that it
      (hopefully) still had in store.
    </p>
    <p>
      This system was originally designed (I think) for applications
      in which the browser itself was going to end up using the
      private key and certificate, i.e. TLS client certificates. But
      it was often reused for applications which have nothing to do
      with that, in which case, after the browser had combined the
      private key with the certificate, you’d then export the result
      into a file containing both the private key and the certificate,
      which you could transfer into whatever application ultimately
      needed to sign things (a web server, or Authenticode, or
      whatever).
    </p>
    <p>That was an undesirable way of doing things for many reasons:
      <ul>
        <li>The browser UI for extracting the key after the
        certificate was delivered was tricky, and easy to get wrong.</li>
        <li>At least in Firefox, the UI provided no way to extract the
        private key <em>before</em> the certificate was delivered, or
        even to see very easily whether it was still there. You just
        had to hope that in a couple of weeks’ time, when your
        certificate was issued, the browser would still be able to
        find the other half of your key.</li>
        <li>You had no option but to use the browser’s key generation
        and key storage. If you wanted to generate your key on a
        hardware device instead, the browser provided no way to do
        that.</li>
        <li>Who said you even wanted to trust the browser with your
        private key <em>at all</em>? Standard principle of sensitive
        data is that you allow as few things as possible to even see
        it. In the original case of TLS client certificates, where the
        browser itself would end up <em>signing</em> things, then of
        course the browser would have to know the private key – but in
        any application that involves exporting the key from the
        browser and using it somewhere else, you’d surely prefer that
        the browser had never known it in the first place.</li>
      </ul>
    </p>
    <p>
      In 2016 the <code>&lt;keygen&gt;</code> element was deprecated,
      and later, removed from the HTML standard. Modern browsers have
      also dropped support for it – for example, it was removed in
      Firefox 69, released in 2019.
    </p>
    <p>
      Unfortunately, as of 2021, some CAs <em>still</em> depend on it
      for (e.g.) Authenticode, and haven’t updated their procedures to
      allow certificate applications to be made any other way! Such a
      CA might suggest, for example, that you use an older browser
      such as IE11 to work around the lack
      of <code>&lt;keygen&gt;</code> in anything up to date. That
      means trusting an <em>obsolete</em> browser, <em>out of security
      support</em>, with a private key of high value.
    </p>
    <p>
      If you’d rather not run that risk, here’s an alternative.
    </p>
    <h2>Overview</h2>
    <p>
      The tools here allow you to use a modern browser, and still
      successfully submit a form containing
      a <code>&lt;keygen&gt;</code>, by <em>pretending</em> the
      browser supports it. This is done by running a Javascript
      bookmarklet after the page has loaded, which finds
      the <code>&lt;keygen&gt;</code> and replaces it with a
      simple <code>&lt;input type="hidden"&gt;</code> containing the
      same response data that a real <code>&lt;keygen&gt;</code> would
      have sent.
    </p>
    <p>
      That response data is known as an SPKAC: “Subject Public Key And
      Challenge”. It’s a blob of encoded data that includes not only
      the public key, but a <em>signature</em> made by that key on a
      challenge string embedded in the <code>&lt;keygen&gt;</code>
      itself. For example, the server might present a web page
      including something like <code>&lt;keygen name="pubkey"
      challenge="12345678"&gt;</code>, and then the browser would have
      to submit a signature covering the string “12345678”.
    </p>
    <p>
      (The purpose of the challenge string is that it can be different
      every time, so that the web server can be sure that the person
      submitting the public key really did have access to the matching
      private key <em>right now</em>, and didn’t prepare the
      submission in advance and then leave it somewhere an attacker
      could substitute a different public key. But not every website
      will care about making sure of this, so you may find that the
      challenge is just the empty string.)
    </p>
    <p>
      This means that you can’t generate the correct SPKAC to respond
      to a <code>&lt;keygen&gt;</code>, without being able to use the
      private key. So the JS bookmarklet can’t do the whole job,
      because you wouldn’t want that bookmarklet to have direct access
      to your real private key. (Not even if you trust <em>me</em>
      completely – the bookmarklet runs in the context of an untrusted
      web page which <em>certainly</em> shouldn’t be able to get hold
      of the private key, even by accident.)
    </p>
    <p>
      So, instead, you have to run a tool outside your browser which
      generates the SPKAC. That tool needs to be able to use the
      private key (one way or another), and also needs to know the
      challenge string from the <code>&lt;keygen&gt;</code> element.
    </p>
    <p>
      Multiple SPKAC generation tools are provided, depending on where
      your private key lives:
      <ul>
        <li><code>gen_ossl_spkac.py</code> uses the
        Unix <code>openssl</code> command-line utility to generate the
        signature. You can use this if you just want to keep the
        private key on disk, in PEM format.</li>
        <li><code>gen_p11_spkac.py</code> uses the
        Unix <code>pkcs11-tool</code> command-line utility to generate
        a signature from a key stored on a hardware device, provided
        that device comes with a PKCS#11 library. For example, this
        script can generate an SPKAC from a key stored on a Yubikey,
        using the library in the <code>ykcs11</code> Debian
        package.</li>
        <li><code>gen_gnupg_spkac.py</code> uses GnuPG to generate the
        signature, from any RSA public key that GnuPG knows about.</li>
      </ul>
    </p>
    <p>
      So the expected workflow would be something like this:
      <ul>
        <li>Browser presents a form containing a
        <code>&lt;keygen&gt;</code>.</li>
        <li>You trigger the bookmarklet. It automatically finds
        the <code>&lt;keygen&gt;</code> on the page, extracts the
        challenge string, and puts up a prompt like this:
          <img src="prompt.png" alt='‘Please enter a base64-encoded SPKAC for challenge "12345678"’' title="This is an image, not a real Firefox dialog box"/>
        </li>
        <li>In a terminal window, you run one of the SPKAC generation
        scripts, using a command like one of these, and pasting the
        challenge string (if any) from the above prompt:
          <pre>./gen_ossl_spkac.py --challenge="12345678" mykey.pem<br/>./gen_p11_spkac.py --challenge="12345678"  /usr/lib/x86_64-linux-gnu/libykcs11.so 1<br/>./gen_gnupg_spkac.py --challenge="12345678" FB8E30B448D4C53E3552E86385116503A8DF59A1</pre>
        If that tool successfully finds and uses your key, it will
        write a pile of base64 data to the terminal, looking something
        like this:
          <pre>MIICSDCCATAwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDa2clucat33ysjIWMsicHc
j0mnhLsGkF2F4rkY0qeMIzzecnuZsQWcTgI8EFOD+yLsEENYdV4RBF1cTqcOmmQG2jaAcgwqV9Lf
nQjgwooMmt45dBPqV/9DtAh2Ah+KMGdKg0hQo3bfppKCgKq+RCFIuAX+6ldgn2KdH6YuN/e3ykn1
Khcke49LQnA03QU/Y/VtLEG/XuPYR4SezwtrDF7zIGGFumg3y8gnjSuia4LH8cZ8vJqfkJtE4ebT
7GedIRT8XHr8Sjp1zjWy9JuI6ijd5Kpvbar/ngmT52sk/yEnqHn2tZFlSmv5+W9qiCGsUEmm9UPi
Aoe3qQGzUdxfunsnAgMBAAEWCDEyMzQ1Njc4MA0GCSqGSIb3DQEBBAUAA4IBAQBpVXbsUAxHZTKo
XV1/uMpcVI3OOWa0/f0iDbDva5oI0Oigtb2+jgt+m2XRZiZnS3ynoTj35qP1ZMUaZuj0ml29PqWQ
giw02Mr6fS1jxMehD8PIrjKyqNvCFWVerbgVmMdO+lyG2KUU70Ec85yZmzhdO7jgQtWmp75VJqMD
Hay+5oFgHAwgfkn9ubghs4cff9VkWI1nDfyEiyLDG5WLAdpftJbu+rwjiFrnAlcL2riB598FW+5Z
NjkrrQO5M/juBXs+WdyLzSP3xWRFMRuRLn1hBZBQ0j4XLjcl+gLgn1obQ9bDye3cJeMD++okRgyn
j0tUnKbOXBDJlc63IO9CcyxP</pre>
        </li>
        <li>Copy and paste all of that data into the prompt in the web
        page, and click “OK”. (In Firefox, at least, you don’t need to
        worry about the line breaks.) You should see another
        Javascript alert saying “Done”.</li>
        <li>Now, when you submit the form as a whole to the original
        web server, it should upload the SPKAC you just generated,
        exactly as if the browser had generated a key. But the SPKAC
        it uploads will refer to whatever key you used with the
        generation script – whether it’s in a file you know where to
        find, or in a hardware token of some sort.</li>
      </ul>
    </p>
    <h2>More details of each generation script</h2>
    <p>
      Here’s some more discussion of each SPKAC generation script, and
      a fully worked example of generating a key and then generating
      an SPKAC from it. Click to expand whichever one you need to know
      more about.
    </p>
    <details>
      <summary><h3>OpenSSL: <code>gen_ossl_spkac.py</code></h3></summary>
      <p>The OpenSSL library comes with a command-line tool
      called <code>openssl</code>, which can generate RSA keys. You’ll
      have to have this installed anyway, because it’s the same tool
      that <code>gen_ossl_spkac.py</code> will use to generate the
      signature in the SPKAC.</p>
      <p>To generate a new 3072-bit RSA key (for example) and save it
      unencrypted to a file on disk, run a command like this:</p>
      <pre>openssl genrsa -out mykey.pem 3072</pre>
      <p>If you want to encrypt the key as well, add a cipher option.
      OpenSSL supports a lot of these (try <code>openssl help</code>
      to see the full list). An example might be:</p>
      <pre>openssl genrsa -aes-256-cbc -out mykey.pem 3072</pre>
      <p>The resulting PEM file will be mostly base64-encoded, and
      start with a line like “<code>-----BEGIN RSA PRIVATE
      KEY-----</code>”. If you’ve encrypted the key, then immediately
      after that header you can also expect to see some headers
      describing the encryption, so that the file might start like
      this:</p>
      <pre>-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: AES-256-CBC,17BA61F07B6EDEA0FE081901296A715D</pre>
      <p>This PEM format for private keys is the same one that OpenSSH
      has historically used for SSH private keys. So an alternative
      way to generate a compatible key is to
      use <code>ssh-keygen</code>, although with modern versions you
      have to use the <code>-m pem</code> option to stop it from
      writing out OpenSSH’s newer and more SSH-specific file format.
      For example, you might replace that generation command with:</p>
      <pre>ssh-keygen -t rsa -b 3072 -m pem -f mykey.pem</pre>
      <p>(Of course, if you prefer, you could also use any other SSH
      key generator that can output in PEM format!)</p>
      <p>Once you’ve generated your key, all the SPKAC generation tool
      needs to know is the challenge string from
      the <code>&lt;keygen&gt;</code>, and where to find the PEM file.
      So you might run, for example:</p>
      <pre>./gen_ossl_spkac.py --challenge="12345678" mykey.pem</pre>
      <p>If the key is encrypted, you’ll be prompted for its
      passphrase <em>twice</em> (because the script has to run two
      separate <code>openssl</code> subcommands, and the second one
      can’t reuse the decrypted key from the first). After that, the
      SPKAC should be written to your terminal.</p>
    </details>
    <details>
      <summary><h3>PKCS#11: <code>gen_p11_spkac.py</code></h3></summary>
      <p>Many hardware cryptography devices provide a driver in the
      form of a <code>.so</code> shared library that speaks the
      PKCS#11 API. <code>pkcs11-tool</code> will talk to those,
      generate keys, and make signatures.</p>
      <p>For example, if you have a Yubikey, then installing the
      Debian (or Ubuntu) package <code>ykcs11</code> will provide a
      file called <code>libykcs11.so</code> in a subdirectory
      of <code>/usr/lib</code>. On a typical x86-64 Linux system the
      exact location will
      be <code>/usr/lib/x86_64-linux-gnu/libykcs11.so</code>.</p>
      <p>To generate a key using <code>pkcs11-tool</code>, you can run
      a command such as</p>
      <pre>pkcs11-tool --module /usr/lib/x86_64-linux-gnu/libykcs11.so --login --login-type so --keypairgen --id 1 --key-type rsa:2048</pre>
      <p>This will prompt you for the “SO PIN”. (“SO” stands for
      “Security Officer” and is the PKCS#11 term for “administrator”:
      typically a device like this has more than one PIN unlocking
      different levels of access.) On a Yubikey, for example, this is
      the thing that <code>ykman</code> thinks of as the “Management
      Key”.</p>
      <p>If that command completes successfully, then the hardware
      token will now hold a public and private key object under the
      specified id (in this case, “1”). The token won’t let you
      extract the private key (that’s most of the point!), but further
      commands with <code>pkcs11-tool</code> will let you extract the
      public key, or ask the token to sign things for you. And other
      software can talk to the same library to generate signatures in
      the context where you’ll eventually want to use the key.</p>
      <p>The SPKAC generation tool needs to know the name of the
      PKCS#11 driver library, and the id of the key object you just
      generated (the same as you gave to the <code>--id</code> option
      in the command above). And, of course, the challenge string from
      the <code>&lt;keygen&gt;</code>:</p>
      <pre>./gen_p11_spkac.py --challenge="12345678" /usr/lib/x86_64-linux-gnu/libykcs11.so 1</pre>
      <p>This time, you’ll likely be prompted for a less privileged
      PIN to log in to the card with, like the “User PIN”. Enter that
      in order to authorise the script to generate a signature from
      the key, and your base64-encoded SPKAC will be printed to the
      terminal.</p>
    </details>
    <details>
      <summary><h3>GnuPG: <code>gen_gnupg_spkac.py</code></h3></summary>
      <p>
        Perhaps you already have a key pair stored in GnuPG and you
        want to use that.
      </p>
      <p>
        You can generate an SPKAC from a GnuPG RSA key by providing
        the key fingerprint (or its shorter 16-digit id) and the
        challenge string from the <code>&lt;keygen&gt;</code>:
      </p>
      <pre>./gen_gnupg_spkac.py --challenge="12345678" FB8E30B448D4C53E3552E86385116503A8DF59A1</pre>
      <p>
        If this requires a key passphrase, the GnuPG agent should
        prompt you out of band for it with a GUI dialog box, in the
        same way as a normal GPG signing operation would.
      </p>
      <p>
        If the key you want to use is a subkey of some other key, you
        can find out its fingerprint using “<code>gpg --list-keys
        --with-subkey-fingerprint</code>”.
      </p>
      <p>
        GnuPG is able to store private keys either in normal disk
        files or on hardware devices. <em>In principle</em>, this tool
        should work the same way in both cases, because you ask the
        GnuPG agent to make a signature in the same way regardless.
        However, you may find that this doesn’t work for a key on a
        hardware device, because the SPKAC requires a signature on an
        MD5 hash, and some PGP-compatible hardware devices will only
        generate signatures on hashes they approve of. For example,
        the OpenPGP application on a Yubikey will fail in this way,
        even though the same signature technique is quite happy to
        sign SHA-1 or SHA-256 hashes. (And even though the same
        Yubikey can sign MD5 hashes with a key you can use through its
        PKCS#11 mode!)
      </p>
    </details>
    <h2>Getting the tools</h2>
    <p>
      The Javascript bookmarklet is right here. Right-click and “Bookmark Link”:
      <a href='javascript:var kgs=document.getElementsByTagName("keygen");if(kgs.length==0){alert("No &lt;keygen&gt; element to fill in");}else if(kgs.length&gt;1){alert("Multiple &lt;keygen&gt; elements");}else{var kg=kgs[0];var ch=kg.getAttribute("challenge");if(ch===undefined||ch===null)ch="";var sp=prompt("Please enter a base64-encoded SPKAC for challenge \""+ch+"\"");sp=sp.replace(/[ \r\n]/g,"");var hi=document.createElement("input");hi.setAttribute("type","hidden");hi.setAttribute("name",kg.getAttribute("name"));hi.setAttribute("value",sp);kg.parentElement.replaceChild(hi,kg);alert("Done");}'>keygen-fake</a>
    </p>
    <p>
      To get the Python scripts that generate an SPKAC to use as the
      response data:
    </p>
    <pre>git clone https://git.tartarus.org/simon/keygen-fake.git</pre>
    <h2>Limitations</h2>
    <p>
      This is not polished software. It has lots of limitations, and
      it hasn’t had a lot of testing. Among the limitations:
      <ul>
        <li>The SPKAC generation scripts have only been tested on
        Linux.</li>
        <li>The bookmarklet has only been tested on Firefox (on
        Linux).</li>
        <li>In principle, <code>&lt;keygen&gt;</code> should be able
        to support more than one public-key algorithm. But these tools
        only handle RSA.</li>
        <li>The PKCS#11 script has only been tested with a Yubikey
        and <code>ykcs11</code>. I have no idea what other PKCS#11
        drivers it might or might not work with, or how to fix it if
        it doesn’t, or how to test it.</li>
        <li>I’m sure there are reasonable places you might keep your
        private key that I don’t have a generation script for. More
        generation scripts welcome.</li>
        <li>The target website might auto-detect your browser as one
        that doesn’t support <code>&lt;keygen&gt;</code>, and try to
        stop you from submitting the form in some way. You might have
        to work around this by additional messing around with the
        page. For example, when I used this on a live website, I had
        to use the Firefox console to delete an <code>onclick</code>
        handler on the form’s “Submit” button, which had been
        installed at page load time based on my browser
        identification, and was preventing me from actually clicking
        the button. I have no way to automate this step for you, or
        even to predict how it will work on some other website, so
        you’ll just have to figure it out for whatever page you’re
        presented with.</li>
      </ul>
      However, in spite of all that, I’ve used it successfully once,
      so it might work for you too!
    </p>
    <h2>Feedback</h2>
    <p>
      Send feedback to
      <a href="mailto:anakin@pobox.com"><code>anakin@pobox.com</code></a>.
    </p>
    <p>
      I don’t expect to have a lot of time to polish this code, so if
      you want it improved, be prepared to help. Patches welcome!
    </p>
  </body>
</html>
