#!/usr/bin/env python3

import argparse
import base64
import binascii
import subprocess
import tempfile

import spkac

class PKCS11KeyClient:
    def __init__(self, args):
        self.module = args.module
        self.keyid = args.id
        # FIXME: other possible parameters like slots

        self.cmd_prefix = ["pkcs11-tool",
                           "--module=" + self.module,
                           "--id=" + self.keyid]

    def get_public_key(self):
        encoded_spki = subprocess.check_output(self.cmd_prefix + [
            "--type=pubkey", "--read-object"])
        spki, _ = spkac.decode(
            encoded_spki, asn1Spec=spkac.SubjectPublicKeyInfo())
        return spkac.pubkey_from_spki(spki)

    def sign_raw_data(self, data):
        with tempfile.NamedTemporaryFile("wb") as infile:
            infile.write(data)
            infile.flush()
            with tempfile.NamedTemporaryFile("rb") as outfile:
                subprocess.check_call(self.cmd_prefix + [
                    "--sign",
                    "--input-file=" + infile.name,
                    "--output-file=" + outfile.name])
                sigdata = outfile.read()
        return int(binascii.hexlify(sigdata), 16)

def main():
    parser = argparse.ArgumentParser(
        description='Generate a SPKAC for a public key available from a '
        'PKCS11 provider.')
    parser.add_argument("module", help="PKCS11 provider library.")
    parser.add_argument("id", help="Object id.")
    parser.add_argument("--challenge", default="",
                        help="Challenge to go in the SPKAC.")
    args = parser.parse_args()

    p11 = PKCS11KeyClient(args)
    modulus, exponent = p11.get_public_key()

    pkac = spkac.make_pkac(modulus, exponent, args.challenge)
    sig_payload = spkac.make_pkcs15_payload(pkac)
    signature = p11.sign_raw_data(sig_payload)
    encoded_spkac = spkac.encode(spkac.make_spkac(pkac, signature))
    spkac.check_spkac(encoded_spkac, args.challenge)
    print(base64.encodebytes(encoded_spkac).decode("ASCII"))

if __name__ == '__main__':
    main()
