#!/usr/bin/env python3

import argparse
import base64
import binascii
import io
import subprocess
import tempfile

import spkac

def parse_key_data(data):
    is_rsa, fp, keygrip, modulus, exponent = (None,) * 5
    for line in data.splitlines():
        fields = line.split(b':')
        if fields[0] in {b'pub', b'sub'}:
            # Introduces a new set of key data. Check it's the right algorithm.
            is_rsa = (fields[3] == b'1')
        if fields[0] == b'fpr':
            fp = fields[9].decode('ASCII')
        if fields[0] == b'grp':
            keygrip = fields[9].decode('ASCII')
        if fields[0:2] == [b'pkd', b'0']:
            modulus = int(fields[3], 16)
        if fields[0:2] == [b'pkd', b'1']:
            exponent = int(fields[3], 16)
        if (is_rsa is not None and fp is not None and keygrip is not None and
            modulus is not None and exponent is not None):
            yield is_rsa, fp, keygrip, modulus, exponent
            is_rsa, fp, keygrip, modulus, exponent = (None,) * 5

def find_rsa_key(query_fp, gpg):
    data = subprocess.check_output([gpg, "--list-keys", "--with-key-data",
                                    query_fp])
    for is_rsa, fp, keygrip, modulus, exponent in parse_key_data(data):
        if fp[-len(query_fp):] == query_fp:
            if is_rsa:
                return keygrip, modulus, exponent
            else:
                raise KeyError('Fingerprint {} identifies a non-RSA key'
                               .format(query_fp))
    raise KeyError('Fingerprint {} not found'.format(query_fp))

def assuan_unescape(data):
    out = []
    it = iter(data)
    for byte in it:
        if byte == 0x25: # '%'
            hexval = chr(next(it))
            hexval += chr(next(it))
            out.append(int(hexval, 16))
        else:
            out.append(byte)
    return bytes(out)

def parse_gpg_list_recurse(data, pos):
    if pos >= len(data):
        raise ValueError('unexpected EOF in gpg list data')
    if data[pos:pos+1] == b'(':
        toret = []
        pos += 1
        while True:
            if pos >= len(data):
                raise ValueError('unexpected EOF in gpg list data')
            if data[pos:pos+1] == b')':
                pos += 1
                return pos, toret
            pos, obj = parse_gpg_list_recurse(data, pos)
            toret.append(obj)
    colonpos = data.index(b':', pos)
    length = int(data[pos:colonpos], 10)
    datapos = colonpos + 1
    endpos = datapos + length
    return endpos, data[datapos:endpos]

def parse_gpg_list(data):
    pos, obj = parse_gpg_list_recurse(data, 0)
    if pos != len(data):
        raise ValueError('trailing junk after gpg list data')
    return obj

def sign_md5_hash(keygrip, hexhash, gpg_connect_agent):
    p = subprocess.Popen([gpg_connect_agent],
                         stdin=subprocess.PIPE, stdout=subprocess.PIPE)

    indata = '''\
sigkey {grip}
sethash --hash=md5 {hash}
pksign
'''.format(grip=keygrip, hash=hexhash).encode('ASCII')
    output, _ = p.communicate(indata)
    p.wait()

    outlines = output.splitlines()
    if outlines[0] != b'OK':
        raise ValueError('gpg-connect-agent rejected the keygrip: {!r}'
                         .format(outlines[0]))
    if outlines[1] != b'OK':
        raise ValueError('gpg-connect-agent rejected the hash: {!r}'
                         .format(outlines[1]))
    if not outlines[2].startswith(b'D '):
        raise ValueError('gpg-connect-agent failed to sign: {!r}'
                         .format(outlines[2]))
    escaped_data = outlines[2][2:] # strip off "D "
    parsed = parse_gpg_list(assuan_unescape(escaped_data))
    x = parsed

    if not (len(x) == 2 and x[0] == b'sig-val'):
        raise ValueError('signature data missing sig-val: {!r}'.format(parsed))
    x = x[1]

    if not (len(x) == 2 and x[0] == b'rsa'):
        raise ValueError('signature data missing rsa: {!r}'.format(parsed))
    x = x[1]

    if not (len(x) == 2 and x[0] == b's'):
        raise ValueError('signature data missing s: {!r}'.format(parsed))
    x = x[1]

    return int(binascii.hexlify(x), 16)

def main():
    parser = argparse.ArgumentParser(
        description='Generate a SPKAC for an RSA private key known to GnuPG.')
    parser.add_argument("id", help="Fingerprint or ID of the key to use.")
    parser.add_argument("--gpg", default="gpg",
                        help="Path to gpg command-line tool.")
    parser.add_argument("--gpg-connect-agent", default="gpg-connect-agent",
                        help="Path to gpg-connect-agent command-line tool.")
    parser.add_argument("--challenge", default="",
                        help="Challenge to go in the SPKAC.")
    args = parser.parse_args()

    # First, find the public-key data and the 'keygrip' value.
    keygrip, modulus, exponent = find_rsa_key(args.id, args.gpg)

    pkac = spkac.make_pkac(modulus, exponent, args.challenge)
    sig_hash = spkac.make_pkcs15_hash_hex(pkac)

    signature = sign_md5_hash(keygrip, sig_hash, args.gpg_connect_agent)
    encoded_spkac = spkac.encode(spkac.make_spkac(pkac, signature))
    spkac.check_spkac(encoded_spkac, args.challenge)
    print(base64.encodebytes(encoded_spkac).decode("ASCII"))

if __name__ == '__main__':
    main()
