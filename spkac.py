#!/usr/bin/env python3

import argparse
import base64
import binascii
import hashlib
import itertools
import sys

import pyasn1
from pyasn1.codec.der.encoder import encode
from pyasn1.codec.der.decoder import decode
from pyasn1.type.univ import (
    Sequence, ObjectIdentifier, BitString, OctetString, Integer, Any, Null)
from pyasn1.type.char import IA5String
from pyasn1.type.namedtype import NamedTypes, NamedType, OptionalNamedType

class AlgorithmIdentifier(Sequence):
    componentType = NamedTypes(
        NamedType('algorithm', ObjectIdentifier()),
        NamedType('parameters', Any()),
    )

rsaalg = AlgorithmIdentifier()
rsaalg['algorithm'] = ObjectIdentifier((1, 2, 840, 113549, 1, 1, 1))
rsaalg['parameters'] = Null()

md5alg = AlgorithmIdentifier()
md5alg['algorithm'] = ObjectIdentifier((1, 2, 840, 113549, 2, 5))
md5alg['parameters'] = Null()

rsamd5alg = AlgorithmIdentifier()
rsamd5alg['algorithm'] = ObjectIdentifier((1, 2, 840, 113549, 1, 1, 4))
rsamd5alg['parameters'] = Null()

class SubjectPublicKeyInfo(Sequence):
    componentType = NamedTypes(
        NamedType('algorithm', AlgorithmIdentifier()),
        NamedType('subjectPublicKey', BitString()),
    )

class PublicKeyAndChallenge(Sequence):
    componentType = NamedTypes(
        NamedType('spki', SubjectPublicKeyInfo()),
        NamedType('challenge', IA5String()),
    )

class SignedPublicKeyAndChallenge(Sequence):
    componentType = NamedTypes(
        NamedType('publicKeyAndChallenge', PublicKeyAndChallenge()),
        NamedType('signatureAlgorithm', pyasn1.type.univ.Any()),
        NamedType('signature', pyasn1.type.univ.BitString()),
    )

class BareRSAPublicKey(Sequence):
    componentType = NamedTypes(
        NamedType('modulus', Integer()),
        NamedType('exponent', Integer()),
    )

class PKCS15Payload(Sequence):
    componentType = NamedTypes(
        NamedType('algorithm', AlgorithmIdentifier()),
        NamedType('digest', OctetString()),
    )

def bytes_to_bitstring(data):
    return BitString("0x" + binascii.hexlify(data).decode("ASCII"))

def make_pkac(modulus, exponent, challenge=""):
    key = BareRSAPublicKey()
    key['modulus'] = modulus
    key['exponent'] = exponent

    spki = SubjectPublicKeyInfo()
    spki['algorithm'] = rsaalg
    spki['subjectPublicKey'] = bytes_to_bitstring(encode(key))

    pkac = PublicKeyAndChallenge()
    pkac['spki'] = spki
    pkac['challenge'] = challenge

    return pkac

def make_pkcs15_hash_hex(pkac):
    return hashlib.md5(encode(pkac)).hexdigest()

def make_pkcs15_payload(pkac):
    payload = PKCS15Payload()
    payload['algorithm'] = md5alg
    payload['digest'] = hashlib.md5(encode(pkac)).digest()

    return encode(payload)

def make_pkcs15_preimage(modulus, payload):
    preimage = int(binascii.hexlify(payload), 16)

    keybits = next(n for n in itertools.count(0, 8) if modulus >> n == 0)
    preimage += 1 << (keybits-15)
    preimage -= 1 << ((len(payload) + 1) * 8)

    return preimage

class SPKACCheckError(Exception): pass

def pubkey_from_spki(spki):
    if encode(spki['algorithm']) != encode(rsaalg):
        raise SPKACCheckError("Not an RSA public key")
    decoded_pubkey, tail = decode(spki['subjectPublicKey'].asOctets(),
                                  asn1Spec=BareRSAPublicKey())
    modulus = int(decoded_pubkey['modulus'])
    exponent = int(decoded_pubkey['exponent'])

    return modulus, exponent

def pubkey_from_pkac(pkac):
    return pubkey_from_spki(pkac['spki'])

def make_spkac(pkac, signature):
    # Re-decode the PKAC to get the RSA key components back out.
    modulus, exponent = pubkey_from_pkac(pkac)

    # Check that the provided signature actually matches.
    payload = make_pkcs15_payload(pkac)
    preimage = make_pkcs15_preimage(modulus, payload)

    decoded_sig = pow(signature, exponent, modulus)
    if decoded_sig != preimage:
        raise SPKACCheckError(
            "Signature does not match!\nexpected = {:#x}\n"
            "received = {:#x}".format(preimage, decoded_sig))

    # Now we can build up the full SignedPublicKeyAndChallenge.
    spkac = SignedPublicKeyAndChallenge()
    spkac['publicKeyAndChallenge'] = pkac
    spkac['signatureAlgorithm'] = rsamd5alg
    spkac['signature'] = "{:#x}".format(signature)

    return spkac

def check_spkac(encoded_spkac, challenge=None):
    spkac, tail = decode(encoded_spkac, asn1Spec=SignedPublicKeyAndChallenge())
    if tail != b"":
        raise SPKACCheckError("Trailing junk after SPKAC")
    pkac = spkac['publicKeyAndChallenge']
    modulus, exponent = pubkey_from_pkac(pkac)
    if encode(spkac['signatureAlgorithm']) != encode(rsamd5alg):
        raise SPKACCheckError("Not an RSA/MD5 signature")
    signature = spkac['signature'].asInteger()
    if challenge is not None and challenge != str(pkac['challenge']):
        raise SPKACCheckError("Challenges do not match")
    new = make_spkac(make_pkac(modulus, exponent, pkac['challenge']), signature)
    if encode(new) != encoded_spkac:
        raise SPKACCheckError("Re-encoding SPKAC did not match")

def main():
    anyint = lambda x: int(x, 0)

    parser = argparse.ArgumentParser(description='Encode a test SPKAC')
    parser.add_argument("modulus", type=anyint, help="RSA key modulus.")
    parser.add_argument("exponent", type=anyint, help="RSA key exponent.")
    parser.add_argument("signature", nargs="?", type=anyint,
                        help="Signature of PKAC. If not supplied, the PKAC "
                        "will be written out for signing.")
    parser.add_argument("--challenge", default="", help="Challenge string.")
    parser.add_argument("--raw", action="store_true",
                        help="Do not base64-encode the output.")
    args = parser.parse_args()

    def output(x):
        if args.raw:
            sys.stdout.buffer.write(x)
        else:
            sys.stdout.write(base64.encodebytes(x).decode("ASCII"))

    pkac = make_pkac(args.modulus, args.exponent, args.challenge)

    if args.signature is None:
        output(encode(pkac))
    else:
        try:
            spkac = make_spkac(pkac, args.signature)
        except SPKACSignatureError as e:
            sys.exit(str(e))

        output(encode(spkac))

        check_spkac(encode(spkac), args.challenge)

if __name__ == '__main__':
    main()
