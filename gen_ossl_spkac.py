#!/usr/bin/env python3

import argparse
import base64
import binascii
import subprocess
import tempfile

import spkac

def main():
    parser = argparse.ArgumentParser(
        description='Generate a SPKAC for an RSA private key stored in PEM '
        'format.')
    parser.add_argument("privkey", help="PEM private key file.")
    parser.add_argument("--openssl", default="openssl",
                        help="Path to openssl command-line tool.")
    parser.add_argument("--challenge", default="",
                        help="Challenge to go in the SPKAC.")
    args = parser.parse_args()

    encoded_spki = subprocess.check_output([
        args.openssl, "rsa", "-inform", "pem", "-in", args.privkey,
        "-pubout", "-outform", "DER"])
    spki, _ = spkac.decode(
        encoded_spki, asn1Spec=spkac.SubjectPublicKeyInfo())
    modulus, exponent = spkac.pubkey_from_spki(spki)

    pkac = spkac.make_pkac(modulus, exponent, args.challenge)
    sig_payload = spkac.make_pkcs15_payload(pkac)

    p = subprocess.Popen([
        args.openssl, "rsautl", "-keyform", "pem", "-inkey", args.privkey,
        "-pkcs", "-sign"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    sigdata, _ = p.communicate(sig_payload)
    p.wait()

    signature = int(binascii.hexlify(sigdata), 16)
    encoded_spkac = spkac.encode(spkac.make_spkac(pkac, signature))
    spkac.check_spkac(encoded_spkac, args.challenge)
    print(base64.encodebytes(encoded_spkac).decode("ASCII"))

if __name__ == '__main__':
    main()
